﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Entity.Migrations
{
    public partial class addDataType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedTime",
                table: "LeaveRequest",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 3, 24, 0, 10, 23, 631, DateTimeKind.Local).AddTicks(4669),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 3, 22, 21, 46, 39, 447, DateTimeKind.Local).AddTicks(8438));

            migrationBuilder.AlterColumn<DateTime>(
                name: "LeaveDate",
                table: "LeaveRequest",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(string),
                oldType: "longtext",
                oldNullable: true)
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedTime",
                table: "LeaveRequest",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 3, 24, 0, 10, 23, 631, DateTimeKind.Local).AddTicks(4275),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 3, 22, 21, 46, 39, 447, DateTimeKind.Local).AddTicks(8026));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedTime",
                table: "Class",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 3, 24, 0, 10, 23, 630, DateTimeKind.Local).AddTicks(5569),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 3, 22, 21, 46, 39, 446, DateTimeKind.Local).AddTicks(8954));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedTime",
                table: "Class",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 3, 24, 0, 10, 23, 630, DateTimeKind.Local).AddTicks(4714),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 3, 22, 21, 46, 39, 446, DateTimeKind.Local).AddTicks(8030));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedTime",
                table: "LeaveRequest",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 3, 22, 21, 46, 39, 447, DateTimeKind.Local).AddTicks(8438),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 3, 24, 0, 10, 23, 631, DateTimeKind.Local).AddTicks(4669));

            migrationBuilder.AlterColumn<string>(
                name: "LeaveDate",
                table: "LeaveRequest",
                type: "longtext",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)")
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedTime",
                table: "LeaveRequest",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 3, 22, 21, 46, 39, 447, DateTimeKind.Local).AddTicks(8026),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 3, 24, 0, 10, 23, 631, DateTimeKind.Local).AddTicks(4275));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedTime",
                table: "Class",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 3, 22, 21, 46, 39, 446, DateTimeKind.Local).AddTicks(8954),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 3, 24, 0, 10, 23, 630, DateTimeKind.Local).AddTicks(5569));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedTime",
                table: "Class",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 3, 22, 21, 46, 39, 446, DateTimeKind.Local).AddTicks(8030),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 3, 24, 0, 10, 23, 630, DateTimeKind.Local).AddTicks(4714));
        }
    }
}
