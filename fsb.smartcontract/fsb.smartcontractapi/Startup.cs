﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Entity.Context;
using Entity.Entity;
using fsb.smartcontractapi.Attribute;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Model.Response;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using Repository.Account;
using Repository.Role;
using Repository.UserInfo;
using Microsoft.OpenApi.Models;
using Repository.Company;
using Repository.Department;
using Repository.Class;
using Repository.LeaveRequest;

namespace fsb.smartcontractapi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDistributedMemoryCache();
            services.AddSession();

            //Chuối kết nối database
            string dbConnectionString = Configuration.GetConnectionString("FsbConnection");
            services.AddDbContext<FsbSmartContractContext>(opt => opt.UseMySql(dbConnectionString, ServerVersion.AutoDetect(dbConnectionString)));

            //services.AddCors();
            services.AddIdentity<User, Role>(options => options.SignIn.RequireConfirmedEmail = false)
                .AddEntityFrameworkStores<FsbSmartContractContext>();
            services.AddTransient<IUserInfoRepository, UserInfoRepository>();
            services.AddTransient<IRoleRepository, RoleRepository>();
            services.AddTransient<IAccountRepository, AccountRepository>();
            services.AddTransient<ICompanyRepository, CompanyRepository>();
            services.AddTransient<IDepartmentRepository, DepartmentRepository>();
            services.AddTransient<IClassRepository, ClassRepository>();
            services.AddTransient<ILeaveRequestRepository, LeaveRequestRepository>();

            services.AddControllers();
            services.AddSwaggerGen();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Account API V1");
            });
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            

            app.UseStaticFiles();
            //app.UseJwtUtil();

            app.UseHttpsRedirection();
            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseStatusCodePages(async context =>
            {
                var response = context.HttpContext.Response;

                if (response.StatusCode == StatusCodes.Status404NotFound)
                {
                    Response<string> res = new Response<string>();
                    res.Code = StatusCodes.Status403Forbidden;
                    res.Message = RolesAuthorizationHandler.Status403Forbidden;
                    await response.WriteAsync(JsonConvert.SerializeObject(res));
                }
            });
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            
        }
    }
}
