﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Model.Requests;
using Model.Response;
using Model.Role;
using Repository.Role;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fsb.smartcontractapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoleController : BaseController
    {
        private readonly IRoleRepository _roleRepository;

        public RoleController(
            IRoleRepository roleRepository,
            ILogger<RoleController> logger,
            IConfiguration configuration
            ) : base(configuration, logger)
        {
            _roleRepository = roleRepository;
        }

        [Route("GetListRole")]
        [HttpPost]
        public TableResponse<RoleViewModel> GetListRole(SearchRoleModel search)
        {
            return _roleRepository.GetListRole(search);
        }

        [Route("CreateRole")]
        [HttpPost]
        //[CustomRole(RoleConstants.ADMIN)]
        public Response<string> CreateRole(RoleModel model)
        {
            return _roleRepository.CreateRole(model);
        }

        [Route("DeleteRole")]
        [HttpPost]
        //[CustomRole(RoleConstants.ADMIN)]
        public Response<string> DeleteRole(RoleModel model)
        {
            return _roleRepository.DeleteRole(model);
        }

        [Route("GetRoleById")]
        [HttpPost]
        //[CustomRole(RoleConstants.ADMIN, RoleConstants.ACCOUNTANT_HO, RoleConstants.ACCOUNTANT_BP)]
        public Response<RoleModel> GetRoleById(RoleModel model)
        {
            return _roleRepository.GetRoleById(model);
        }

        [Route("UpdateRole")]
        [HttpPost]
        //[CustomRole(RoleConstants.ADMIN)]
        public Response<string> UpdateRole(RoleModel model)
        {
            return _roleRepository.UpdateRole(model);
        }
    }
}
