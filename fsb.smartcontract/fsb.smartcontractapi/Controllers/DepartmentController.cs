﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Model.Department;
using Model.Requests;
using Model.Response;
using Repository.Department;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fsb.smartcontractapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DepartmentController : BaseController
    {
        private readonly IDepartmentRepository _departmentRepository;

        public DepartmentController(
            IDepartmentRepository departmentRepository,
            ILogger<DepartmentController> logger,
            IConfiguration configuration
            ) : base(configuration, logger)
        {
            _departmentRepository = departmentRepository;
        }
        [Route("GetListDepartment")]
        [HttpPost]
        public TableResponse<DepartmentViewModel> GetListDepartment(SearchDepartmentModel search)
        {
            return _departmentRepository.GetListDepartment(search);
        }
        [Route("CreateDepartment")]
        [HttpPost]
        //[CustomDepartment(DepartmentConstants.ADMIN)]
        public Response<string> CreateDepartment(DepartmentModel model)
        {
            return _departmentRepository.CreateDepartment(model);
        }
        [Route("DeleteDepartment")]
        [HttpPost]
        //[CustomDepartment(DepartmentConstants.ADMIN)]
        public Response<string> DeleteDepartment(DepartmentModel model)
        {
            return _departmentRepository.DeleteDepartment(model);
        }
    }
}
