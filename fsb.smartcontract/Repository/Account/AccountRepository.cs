﻿using Common;
using Entity.Context;
using Entity.Entity;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Model.Response;
using Model.UserInfo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Account
{
    public class AccountRepository : Repository<AccountRepository>, IAccountRepository
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IConfiguration _configuration;

        public AccountRepository(
            ILogger<AccountRepository> logger,
            UserManager<User> userManager,
            IConfiguration configuration,
            FsbSmartContractContext context,
            SignInManager<User> signInManager
            ) : base(context, logger)
        {
            _userManager = userManager;
            _configuration = configuration;
            _signInManager = signInManager;
        }

        public string EncodeSha256(User user, string role)
        {
            string token = null;
            var key = _configuration["Tokens:Key"];
            var issuer = _configuration["Tokens:Issuer"];
            var expires = _configuration["Tokens:Expires"];
            if (string.IsNullOrEmpty(key))
                key = "ASDFGHJKLZXCVBNMQWERTYUIOP";
            if (string.IsNullOrEmpty(issuer))
                issuer = "Vinfast-FinancialPlan";
            if (string.IsNullOrEmpty(expires))
                expires = "1*24*60*60";
            var arr = expires.Split('*');
            double seconds = 1;
            try
            {
                foreach (var item in arr)
                {
                    seconds *= Int32.Parse(item.ToString());
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                seconds = 1 * 24 * 60 * 60;
            }
            List<Claim> claims = new List<Claim>();
            claims.Add(new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()));
            claims.Add(new Claim(ClaimTypes.Role, role == null ? "" : role));
            token = TokenUtil.EncodeSha256(claims, key, issuer, seconds);
            return token;
        }

        public Response<User> Authencate(LoginModel model)
        {
            Response<User> res = new Response<User>();
            try
            {
                var user = _context.Users.Where(x => x.UserName == model.UserName && x.IsDelete == false).FirstOrDefault();
                if (user == null)
                {
                    res.Code = StatusCodes.Status404NotFound;
                    res.Message = "Tài khoản hoặc mật khẩu bị sai!";
                    res.Data = user;
                    return res;
                }
                else
                {
                    res.Code = StatusCodes.Status200OK;
                    res.Message = "Đăng nhập thành công!";
                    res.Data = user;
                    return res;
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
            }
            return res;
        }
    }
}
