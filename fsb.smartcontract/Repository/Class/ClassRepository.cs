﻿using Entity.Context;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Model.Class;
using Model.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Class
{
    public class ClassRepository : Repository<ClassRepository>, IClassRepository
    {
        private readonly IConfiguration _configuration;
        public ClassRepository(
            ILogger<ClassRepository> logger,
            IConfiguration configuration,
            FsbSmartContractContext context
            ) : base(context, logger)
        {
            _configuration = configuration;
        }
        public Response<string> CreateClass(ClassModel model)
        {
            Response<string> res = new Response<string>();

            try
            {
                if (string.IsNullOrEmpty(model.ClassName))
                {
                    res.Code = StatusCodes.Status400BadRequest;
                    res.Message = "Tên lớp học không được bỏ trống!";
                    return res;
                }

                var data = _context.Class.FirstOrDefault(x => x.ClassName == model.ClassName.Trim());
                if (data != null)
                {
                    res.Code = StatusCodes.Status404NotFound;
                    res.Message = "Lớp học đã tồn tại!";
                    return res;
                }

                Entity.Entity.Class classRoom = new Entity.Entity.Class();
                classRoom.ClassName = model.ClassName;


                _context.Class.Add(classRoom);
                _context.SaveChanges();

                res.Code = StatusCodes.Status200OK;
                res.Message = "Thêm lớp học thành công!";
                return res;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Xảy ra lỗi khi thêm lớp học!";
            }

            return res;
        }

        public List<SelectListItem> GetListClassForCombo()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            try
            {
                var _data = _context.Class.Where(x => x.IsDeleted == false)
                    .Select(company => new SelectListItem
                    {
                        Value = company.Id.ToString(),
                        Text = company.ClassName
                    }).ToList();
                list = _data;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Lỗi lấy list");
            }
            return list;
        }
    }
}
