﻿using Entity.Context;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Model.Department;
using Model.Requests;
using Model.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Department
{
    public class DepartmentRepository : Repository<DepartmentRepository>, IDepartmentRepository
    {
        public DepartmentRepository(
            ILogger<DepartmentRepository> logger,
            IConfiguration configuration,
            FsbSmartContractContext context
            ) : base(context, logger)
        {
        }
        public TableResponse<DepartmentViewModel> GetListDepartment(SearchDepartmentModel search)
        {
            TableResponse<DepartmentViewModel> result = new TableResponse<DepartmentViewModel>();
            result.Draw = search.Draw;

            try
            {
                var data = _context.Department.Where(x => x.IsDeleted == false).Select(x => new DepartmentViewModel
                {
                    Id = x.Id,
                    DepartmentCode = x.DepartmentCode,
                    DepartmentName = x.DepartmentName,
                    CompanyName = x.Company.CompanyName
                }).ToList();

                if (search.SearchValue != null)
                {
                    data = data.Where(x => x.DepartmentName.ToLower().Contains(search.SearchValue.ToLower())).ToList();
                }

                var cnt = data.Count();
                result.Data = data.OrderBy(x => x.Id).Skip(search.Start).Take(search.Length).ToList();
                result.RecordsTotal = cnt;
                result.RecordsFiltered = cnt;
                result.Code = StatusCodes.Status200OK;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                result.Code = StatusCodes.Status500InternalServerError;
                result.Message = "Xảy ra lỗi khi lấy danh sách phòng ban!";
            }
            return result;
        }

        public Response<string> CreateDepartment(DepartmentModel model)
        {
            Response<string> res = new Response<string>();

            try
            {
                if (string.IsNullOrEmpty(model.DepartmentName))
                {
                    res.Code = StatusCodes.Status400BadRequest;
                    res.Message = "Tên phòng ban không được bỏ trống!";
                    return res;
                }
                if (string.IsNullOrEmpty(model.DepartmentCode))
                {
                    res.Code = StatusCodes.Status400BadRequest;
                    res.Message = "Mã phòng ban không được bỏ trống!";
                    return res;
                }
                if (model.CompanyId <= 0)
                {
                    res.Code = StatusCodes.Status400BadRequest;
                    res.Message = "Công ty không được bỏ trống!";
                    return res;
                }

                var data = _context.Department.FirstOrDefault(x => x.DepartmentName == model.DepartmentName.Trim() && x.IsDeleted == false);
                if (data != null)
                {
                    res.Code = StatusCodes.Status404NotFound;
                    res.Message = "Phòng ban đã tồn tại!";
                    return res;
                }

                Entity.Entity.Department department = new Entity.Entity.Department();
                department.DepartmentCode = model.DepartmentCode;
                department.DepartmentName = model.DepartmentName;
                department.CompanyId = model.CompanyId;
                _context.Department.Add(department);
                _context.SaveChanges();

                res.Code = StatusCodes.Status200OK;
                res.Message = "Thêm phòng ban thành công!";
                return res;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Xảy ra lỗi khi thêm phòng ban!";
            }

            return res;
        }
        public Response<string> DeleteDepartment(DepartmentModel model)
        {
            Response<string> res = new Response<string>();

            try
            {
                var department = _context.Department.FirstOrDefault(x => x.Id == model.Id && x.IsDeleted == false);
                if (department == null)
                {
                    res.Code = StatusCodes.Status404NotFound;
                    res.Message = "Không tồn tại phòng ban, không thể xóa!";
                    return res;
                }
                department.IsDeleted = true;
                _context.Department.Update(department);
                _context.SaveChanges();

                res.Code = StatusCodes.Status200OK;
                res.Message = "Xóa phòng ban thành công!";
                return res;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Xảy ra lỗi khi xóa phòng ban!";
            }

            return res;
        }
    }
}
