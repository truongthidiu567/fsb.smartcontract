﻿using Entity.Context;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Model.LeaveRequest;
using Model.Requests;
using Model.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.LeaveRequest
{
    public class LeaveRequestRepository : Repository<LeaveRequestRepository>, ILeaveRequestRepository
    {
        private readonly IConfiguration _configuration;
        public LeaveRequestRepository(
            ILogger<LeaveRequestRepository> logger,
            IConfiguration configuration,
            FsbSmartContractContext context
            ) : base(context, logger)
        {
            _configuration = configuration;
        }

        public TableResponse<LeaveRequestViewModel> GetListLeaveRequest(SearchLeaveRequestModel search)
        {
            TableResponse<LeaveRequestViewModel> result = new TableResponse<LeaveRequestViewModel>();
            result.Draw = search.Draw;

            try
            {
                var data = _context.LeaveRequests.Where(x => x.IsDeleted == false).Select(x => new LeaveRequestViewModel
                {
                    Id = x.Id,
                    StudentName = x.StudentName,
                    ClassName = x.Class.ClassName,
                    Title = x.Title,
                    LeaveDate = x.LeaveDate,
                    LeaveType = x.LeaveType,
                    Status = x.Status,
                }).ToList();

                if (search.SearchValue != null)
                {
                    data = data.Where(x => x.StudentName.ToLower().Contains(search.SearchValue.ToLower())).ToList();
                }

                foreach (var item in data)
                {
                    if (item.LeaveType == 1)
                    {
                        item.LeaveTypeName = "Late Coming";
                    }
                    else if (item.LeaveType == 2)
                    {
                        item.LeaveTypeName = "Early Leave";
                    }
                    else if (item.LeaveType == 3)
                    {
                        item.LeaveTypeName = "Leave a haft of day";
                    }
                    else
                    {
                        item.LeaveTypeName = "Leave one day";
                    }

                    if (item.Status == 1)
                    {
                        item.StatusName = "Submitted";
                    }
                    else if (item.Status == 2)
                    {
                        item.StatusName = "Cancel";
                    }
                    else if (item.Status == 3)
                    {
                        item.StatusName = "Approve";
                    }
                    else
                    {
                        item.StatusName = "Reject";
                    }
                }

                var cnt = data.Count();
                result.Data = data.OrderBy(x => x.Id).Skip(search.Start).Take(search.Length).ToList();
                result.RecordsTotal = cnt;
                result.RecordsFiltered = cnt;
                result.Code = StatusCodes.Status200OK;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                result.Code = StatusCodes.Status500InternalServerError;
                result.Message = "Xảy ra lỗi khi lấy danh sách nghỉ phép!";
            }
            return result;
        }

        public Response<string> CreateLeaveRequest(LeaveRequestModel model)
        {
            Response<string> res = new Response<string>();

            try
            {
                if (string.IsNullOrEmpty(model.StudentName))
                {
                    res.Code = StatusCodes.Status400BadRequest;
                    res.Message = "Tên học viên không được bỏ trống!";
                    return res;
                }

                if (model.LeaveType == 0)
                {
                    res.Code = StatusCodes.Status400BadRequest;
                    res.Message = "Loại nghỉ phép không được bỏ trống!";
                    return res;
                }

                if (model.ClassId <= 0)
                {
                    res.Code = StatusCodes.Status400BadRequest;
                    res.Message = "Lớp không được bỏ trống!";
                    return res;
                }

                if (model.Title == null)
                {
                    res.Code = StatusCodes.Status400BadRequest;
                    res.Message = "Tiêu đề không được bỏ trống!";
                    return res;
                }

                Entity.Entity.LeaveRequest leaveRequest = new Entity.Entity.LeaveRequest();
                leaveRequest.StudentName = model.StudentName;
                leaveRequest.ClassId = model.ClassId;
                leaveRequest.Title = model.Title;
                leaveRequest.LeaveDate = model.LeaveDate;
                leaveRequest.LeaveType = model.LeaveType;
                leaveRequest.ReasonLeave = model.ReasonLeave;
                leaveRequest.Status = 1;

                _context.LeaveRequests.Add(leaveRequest);
                _context.SaveChanges();

                res.Code = StatusCodes.Status200OK;
                res.Message = "Thêm ngày nghỉ phép thành công!";
                return res;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Xảy ra lỗi khi thêm ngày nghỉ phép!";
            }

            return res;
        }

        public Response<LeaveRequestModel> GetLeaveRequestById(LeaveRequestModel model)
        {
            Response<LeaveRequestModel> res = new Response<LeaveRequestModel>();

            try
            {
                var query = (from a in _context.LeaveRequests
                             where a.IsDeleted == false && a.Id == model.Id
                             select new
                             {
                                 a
                             }).ToList();

                var group = query.GroupBy(x => x.a.Id).Select(group => new LeaveRequestModel
                {
                    Id = group.Key,
                    StudentName = group.ToList().FirstOrDefault().a.StudentName,
                    ClassId = group.ToList().FirstOrDefault().a.ClassId,
                    Title = group.ToList().FirstOrDefault().a.Title,
                    LeaveDate = group.ToList().FirstOrDefault().a.LeaveDate,
                    LeaveType = group.ToList().FirstOrDefault().a.LeaveType,
                    ReasonLeave = group.ToList().FirstOrDefault().a.ReasonLeave,
                }).ToList();

                res.Code = StatusCodes.Status200OK;
                res.Data = group.FirstOrDefault();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status400BadRequest;
                res.Message = "Xảy ra lỗi khi lấy thông tin công ty!";
            }

            return res;
        }

        public Response<string> UpdateStatusLeaveRequest(LeaveRequestModel model)
        {
            Response<string> res = new Response<string>();

            try
            {

                var leaveRequest = _context.LeaveRequests.FirstOrDefault(x => x.IsDeleted == false && x.Id == model.Id);
                if (leaveRequest == null)
                {
                    res.Code = StatusCodes.Status404NotFound;
                    res.Message = "Không tồn tại lịch nghỉ, không thể cập nhật!";
                    return res;
                }

                leaveRequest.Status = model.Status;
                _context.LeaveRequests.Update(leaveRequest);
                _context.SaveChanges();

                res.Code = StatusCodes.Status200OK;
                res.Message = "Cập nhật trạng thái thành công!";
                return res;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Xảy ra lỗi khi cập nhật trạng thái!";
            }

            return res;
        }

        public Response<string> UpdateLeaveRequest(LeaveRequestModel model)
        {
            Response<string> res = new Response<string>();

            try
            {

                var leaveRequest = _context.LeaveRequests.FirstOrDefault(x => x.IsDeleted == false && x.Id == model.Id);
                if (leaveRequest.Status > 1)
                {
                    res.Code = StatusCodes.Status404NotFound;
                    res.Message = "Không thể cập nhật thông tin vì đã thay đổi trạng thái";
                    return res;
                }

                if (leaveRequest == null)
                {
                    res.Code = StatusCodes.Status404NotFound;
                    res.Message = "Không tồn tại lịch nghỉ, không thể cập nhật!";
                    return res;
                }

                leaveRequest.StudentName = model.StudentName;
                leaveRequest.ClassId = model.ClassId;
                leaveRequest.Title = model.Title;
                leaveRequest.LeaveDate = model.LeaveDate;
                leaveRequest.LeaveType = model.LeaveType;
                leaveRequest.ReasonLeave = model.ReasonLeave;
                _context.LeaveRequests.Update(leaveRequest);
                _context.SaveChanges();

                res.Code = StatusCodes.Status200OK;
                res.Message = "Cập nhật thông tin thành công!";
                return res;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Xảy ra lỗi khi cập nhật thông tin!";
            }

            return res;
        }

        public Response<string> DeleteLeaveRequest(LeaveRequestModel model)
        {
            Response<string> res = new Response<string>();

            try
            {

                var leaveRequest = _context.LeaveRequests.FirstOrDefault(x => x.IsDeleted == false && x.Id == model.Id);
                if (leaveRequest == null)
                {
                    res.Code = StatusCodes.Status404NotFound;
                    res.Message = "Không tồn tại lịch nghỉ, không thể cập nhật!";
                    return res;
                }

                if (leaveRequest.Status > 1)
                {
                    res.Code = StatusCodes.Status404NotFound;
                    res.Message = "Không thể xóa thông tin vì đã thay đổi trạng thái";
                    return res;
                }

                leaveRequest.IsDeleted = true;
                _context.LeaveRequests.Update(leaveRequest);
                _context.SaveChanges();

                res.Code = StatusCodes.Status200OK;
                res.Message = "Xóa trạng thái thành công!";
                return res;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Xảy ra lỗi khi cập nhật trạng thái!";
            }

            return res;
        }
    }
}
