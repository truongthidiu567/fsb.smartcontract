﻿using Entity.Context;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Model.Company;
using Model.Requests;
using Model.Response;
using Model.UserInfo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Company
{
    public class CompanyRepository : Repository<CompanyRepository>, ICompanyRepository
    {
        private readonly IConfiguration _configuration;
        public CompanyRepository(
            ILogger<CompanyRepository> logger,
            IConfiguration configuration,
            FsbSmartContractContext context
            ) : base(context, logger)
        {
            _configuration = configuration;
        }


        public TableResponse<CompanyViewModel> GetListCompany(SearchCompanyModel search)
        {
            TableResponse<CompanyViewModel> result = new TableResponse<CompanyViewModel>();
            result.Draw = search.Draw;

            try
            {
                var data = _context.Company.Where(x => x.IsDeleted == false).Select(x => new CompanyViewModel
                {
                    Id = x.Id,
                    CompanyName = x.CompanyName,
                    CompanyCode = x.CompanyCode
                }).ToList();

                if (search.SearchValue != null)
                {
                    data = data.Where(x => x.CompanyName.ToLower().Contains(search.SearchValue.ToLower())).ToList();
                }

                var cnt = data.Count();
                result.Data = data.OrderBy(x => x.Id).Skip(search.Start).Take(search.Length).ToList();
                result.RecordsTotal = cnt;
                result.RecordsFiltered = cnt;
                result.Code = StatusCodes.Status200OK;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                result.Code = StatusCodes.Status500InternalServerError;
                result.Message = "Xảy ra lỗi khi lấy danh sách công ty!";
            }
            return result;
        }

        public Response<string> CreateCompany(CompanyModel model)
        {
            Response<string> res = new Response<string>();

            try
            {
                if (string.IsNullOrEmpty(model.CompanyName))
                {
                    res.Code = StatusCodes.Status400BadRequest;
                    res.Message = "Tên công ty không được bỏ trống!";
                    return res;
                }
                if (string.IsNullOrEmpty(model.CompanyCode))
                {
                    res.Code = StatusCodes.Status400BadRequest;
                    res.Message = "Mã công ty không được bỏ trống!";
                    return res;
                }

                var data = _context.Company.FirstOrDefault(x => x.CompanyName == model.CompanyName.Trim());
                if (data != null)
                {
                    res.Code = StatusCodes.Status404NotFound;
                    res.Message = "Công ty đã tồn tại!";
                    return res;
                }

                Entity.Entity.Company company = new Entity.Entity.Company();
                company.CompanyName = model.CompanyName;
                company.CompanyCode = model.CompanyCode;
                _context.Company.Add(company);
                _context.SaveChanges();

                res.Code = StatusCodes.Status200OK;
                res.Message = "Thêm công ty thành công!";
                return res;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Xảy ra lỗi khi thêm công ty!";
            }

            return res;
        }

        public Response<string> DeleteCompany(CompanyModel model)
        {
            Response<string> res = new Response<string>();

            try
            {
                var checkUse = _context.Department.Where(x => x.CompanyId == model.Id && x.IsDeleted == false).Count();
                if(checkUse > 0)
                {
                    res.Code = StatusCodes.Status404NotFound;
                    res.Message = "Đã phát sinh nghiệp vụ, không thể xóa bản ghi!";
                    return res;
                }
                var company = _context.Company.FirstOrDefault(x => x.Id == model.Id && x.IsDeleted == false);
                if (company == null)
                {
                    res.Code = StatusCodes.Status404NotFound;
                    res.Message = "Không tồn tại công ty, không thể xóa!";
                    return res;
                }
                company.IsDeleted = true;
                _context.Company.Update(company);
                _context.SaveChanges();

                res.Code = StatusCodes.Status200OK;
                res.Message = "Xóa công ty thành công!";
                return res;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Xảy ra lỗi khi xóa công ty!";
            }

            return res;
        }

        public Response<CompanyModel> GetCompanyById(CompanyModel model)
        {
            Response<CompanyModel> res = new Response<CompanyModel>();

            try
            {
                var query = (from a in _context.Company
                             where a.IsDeleted == false && a.Id == model.Id
                             select new
                             {
                                 a
                             }).ToList();

                var group = query.GroupBy(x => x.a.Id).Select(group => new CompanyModel
                {
                    Id = group.Key,
                    CompanyName = group.ToList().FirstOrDefault().a.CompanyName,
                    CompanyCode = group.ToList().FirstOrDefault().a.CompanyCode
                }).ToList();

                res.Code = StatusCodes.Status200OK;
                res.Data = group.FirstOrDefault();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status400BadRequest;
                res.Message = "Xảy ra lỗi khi lấy thông tin công ty!";
            }

            return res;
        }

        public Response<string> UpdateCompany(CompanyModel model)
        {
            Response<string> res = new Response<string>();

            try
            {
                if (string.IsNullOrEmpty(model.CompanyName))
                {
                    res.Code = StatusCodes.Status400BadRequest;
                    res.Message = "Tên công ty không được bỏ trống!";
                    return res;
                }

                var company = _context.Company.FirstOrDefault(x => x.IsDeleted == false && x.Id == model.Id);
                if (company == null)
                {
                    res.Code = StatusCodes.Status404NotFound;
                    res.Message = "Không tồn tại công ty, không thể cập nhật!";
                    return res;
                }

                company.CompanyName = model.CompanyName;
                company.CompanyCode = model.CompanyCode;
                _context.Company.Update(company);
                _context.SaveChanges();

                res.Code = StatusCodes.Status200OK;
                res.Message = "Sửa nhóm quyền thành công!";
                return res;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Xảy ra lỗi khi sửa nhóm quyền!";
            }

            return res;
        }

        public List<SelectListItem> GetListCompanyForCombo()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            try
            {
                var _data = _context.Company.Where(x => x.IsDeleted == false)
                    .Select(company => new SelectListItem
                    {
                        Value = company.Id.ToString(),
                        Text = company.CompanyName
                    }).ToList();
                list = _data;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Lỗi lấy list");
            }
            return list;
        }


    }
}
