﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Model.Company;
using Model.Requests;
using Model.Response;
using Model.UserInfo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Company
{
    public interface ICompanyRepository
    {
        TableResponse<CompanyViewModel> GetListCompany(SearchCompanyModel search);
        Response<string> CreateCompany(CompanyModel model);
        Response<string> DeleteCompany(CompanyModel model);
        Response<CompanyModel> GetCompanyById(CompanyModel model);
        Response<string> UpdateCompany(CompanyModel model);
        List<SelectListItem> GetListCompanyForCombo();
    }
}
