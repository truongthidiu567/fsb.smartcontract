﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using fsb.smartcontract.Services.Company;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Model.Company;
using Model.Requests;
using Model.Response;
using NToastNotify;

namespace fsb.smartcontract.Controllers
{
    public class CompanyController : BaseController
    {
        private readonly ILogger<CompanyController> _logger;
        private readonly IToastNotification _toastNotification;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ICompanyService _companyService;

        public CompanyController(
            ILogger<CompanyController> logger,
            IHttpClientFactory factory,
            IToastNotification toastNotification,
            IHttpContextAccessor httpContextAccessor,
            ICompanyService companyService
            ) : base(factory)
        {
            _logger = logger;
            _toastNotification = toastNotification;
            _httpContextAccessor = httpContextAccessor;
            _companyService = companyService;
        }
        [Route("danh-sach-cong-ty")]
        public IActionResult Companies()
        {
            return View();
        }

        [HttpPost]
        public IActionResult GetListCompany(string searchValue)
        {
            SearchCompanyModel searchModel = talbeSearchModel.Get<SearchCompanyModel>();
            searchModel.SearchValue = searchValue;
            var data = _companyService.GetListCompany(searchModel);
            if (data.Code != StatusCodes.Status200OK)
                _toastNotification.AddErrorToastMessage(data.Message);
            return Json(data);
        }
        [Route("them-moi-cong-ty")]
        public IActionResult CreateCompany()
        {
            return View();
        }

        [HttpPost]
        public IActionResult InsertCompany(CompanyModel model)
        {
            var res = _companyService.CreateCompany(model);
            if (res.Code == StatusCodes.Status200OK)
            {
                _toastNotification.AddSuccessToastMessage(res.Message);
            }
            else
            {
                _toastNotification.AddErrorToastMessage(res.Message);
            }
            return Json(res);
        }

        [HttpPost]
        public IActionResult DeleteCompany(int Id)
        {
            var model = new CompanyModel();
            model.Id = Id;
            model.UpdateBy = _SessionUser.Id;
            var res = _companyService.DeleteCompany(model);
            if (res.Code == StatusCodes.Status200OK)
            {
                _toastNotification.AddSuccessToastMessage(res.Message);
            }
            else
            {
                _toastNotification.AddErrorToastMessage(res.Message);
            }
            return Json(res);
        }

        [Route("sua-cong-ty/{id}")]
        public IActionResult EditCompany(int Id)
        {
            var model = new CompanyModel();
            model.Id = Id;
            var res = _companyService.GetCompanyById(model);

            if (res == null)
                res = new Response<CompanyModel>();
            if (res.Data == null)
                res.Data = new CompanyModel();

            return View(res.Data);
        }

        [HttpPost]
        public IActionResult UpdateCompany(CompanyModel model)
        {
            var res = _companyService.UpdateCompany(model);
            if (res.Code == StatusCodes.Status200OK)
            {
                _toastNotification.AddSuccessToastMessage(res.Message);
            }
            else
            {
                _toastNotification.AddErrorToastMessage(res.Message);
            }
            return Json(res);
        }
    }
}
