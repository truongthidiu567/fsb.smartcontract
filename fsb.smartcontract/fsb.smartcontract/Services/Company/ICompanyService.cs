﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Model.Company;
using Model.Requests;
using Model.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fsb.smartcontract.Services.Company
{
    public interface ICompanyService
    {
        TableResponse<CompanyViewModel> GetListCompany(SearchCompanyModel search);
        Response<string> CreateCompany(CompanyModel model);
        Response<string> DeleteCompany(CompanyModel model);
        Response<CompanyModel> GetCompanyById(CompanyModel model);
        Response<string> UpdateCompany(CompanyModel model);
        List<SelectListItem> GetListCompanyForCombo();
    }
}
