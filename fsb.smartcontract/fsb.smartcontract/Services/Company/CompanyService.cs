﻿using fsb.smartcontract.Api;
using fsb.smartcontract.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Model.Company;
using Model.Requests;
using Model.Response;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace fsb.smartcontract.Services.Company
{
    public class CompanyService : Service<ICompanyService>, ICompanyService
    {
        public CompanyService(
           CallWebAPI api,
           ILogger<CompanyService> logger,
           IHttpClientFactory factory,
           IHttpContextAccessor context
           ) : base(api, logger, factory, context)
        {
        }

        public TableResponse<CompanyViewModel> GetListCompany(SearchCompanyModel search)
        {
            TableResponse<CompanyViewModel> res = new TableResponse<CompanyViewModel>();

            try
            {
                string url = Path.COMPANY_DATATABLE;
                res = _api.Post<TableResponse<CompanyViewModel>>(url, search);
                if (res.Code != StatusCodes.Status200OK)
                {
                    res.RecordsFiltered = 0;
                    res.RecordsTotal = 0;
                    res.Draw = search.Draw;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Lỗi gọi api!";
            }

            return res;
        }

        public Response<string> CreateCompany(CompanyModel model)
        {
            Response<string> res = new Response<string>();
            try
            {
                string url = Path.COMPANY_CREATE;
                res = _api.Post<Response<string>>(url, model);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Lỗi gọi api!";
            }
            return res;
        }

        public Response<string> DeleteCompany(CompanyModel model)
        {
            Response<string> res = new Response<string>();
            try
            {
                string url = Path.COMPANY_DELETE;
                res = _api.Post<Response<string>>(url, model);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Lỗi gọi api!";
            }
            return res;
        }

        public Response<CompanyModel> GetCompanyById(CompanyModel model)
        {
            Response<CompanyModel> res = new Response<CompanyModel>();
            try
            {
                string url = Path.COMPANY_BYID;
                res = _api.Post<Response<CompanyModel>>(url, model);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Lỗi gọi api!";
            }
            return res;
        }

        public Response<string> UpdateCompany(CompanyModel model)
        {
            Response<string> res = new Response<string>();
            try
            {
                string url = Path.COMPANY_UPDATE;
                res = _api.Post<Response<string>>(url, model);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Lỗi gọi api!";
            }
            return res;
        }
        public List<SelectListItem> GetListCompanyForCombo()
        {
            var response = Client.GetAsync(Path.COMPANY_FORCOMBO).Result;
            string jData = response.Content.ReadAsStringAsync().Result;
            var comboData = JsonConvert.DeserializeObject <List<SelectListItem>>(jData);
            return comboData;
        }

    }
}
