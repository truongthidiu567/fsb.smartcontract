﻿using fsb.smartcontract.Api;
using fsb.smartcontract.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Model.Department;
using Model.Requests;
using Model.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace fsb.smartcontract.Services.Department
{
    public class DepartmentService : Service<IDepartmentService>, IDepartmentService
    {
        public DepartmentService(
           CallWebAPI api,
           ILogger<DepartmentService> logger,
           IHttpClientFactory factory,
           IHttpContextAccessor context
           ) : base(api, logger, factory, context)
        {
        }

        public TableResponse<DepartmentViewModel> GetListDepartment(SearchDepartmentModel search)
        {
            TableResponse<DepartmentViewModel> res = new TableResponse<DepartmentViewModel>();

            try
            {
                string url = Path.DEPARTMENT_DATATABLE;
                res = _api.Post<TableResponse<DepartmentViewModel>>(url, search);
                if (res.Code != StatusCodes.Status200OK)
                {
                    res.RecordsFiltered = 0;
                    res.RecordsTotal = 0;
                    res.Draw = search.Draw;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Lỗi gọi api!";
            }

            return res;
        }

        public Response<string> CreateDepartment(DepartmentModel model)
        {
            Response<string> res = new Response<string>();
            try
            {
                string url = Path.DEPARTMENT_CREATE;
                res = _api.Post<Response<string>>(url, model);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Lỗi gọi api!";
            }
            return res;
        }
        public Response<string> DeleteDepartment(DepartmentModel model)
        {
            Response<string> res = new Response<string>();
            try
            {
                string url = Path.DEPARTMENT_DELETE;
                res = _api.Post<Response<string>>(url, model);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Lỗi gọi api!";
            }
            return res;
        }
    }
}
