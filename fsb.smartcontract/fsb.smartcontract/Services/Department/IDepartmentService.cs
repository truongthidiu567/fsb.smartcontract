﻿using Model.Department;
using Model.Requests;
using Model.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fsb.smartcontract.Services.Department
{
    public interface IDepartmentService
    {
        TableResponse<DepartmentViewModel> GetListDepartment(SearchDepartmentModel search);
        Response<string> CreateDepartment(DepartmentModel model);
        Response<string> DeleteDepartment(DepartmentModel model);
    }
}
