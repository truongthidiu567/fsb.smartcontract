﻿using Common;
using fsb.smartcontract.Api;
using fsb.smartcontract.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace fsb.smartcontract.Services
{
    public abstract class Service<T>
    {
        public static CallWebAPI _api;
        protected static ILogger<T> _logger;
        private IHttpClientFactory _factory;
        private readonly IHttpContextAccessor _context;

        public Service(
            CallWebAPI api,
            ILogger<T> logger,
            IHttpClientFactory factory,
            IHttpContextAccessor context)
        {
            _api = api;
            _logger = logger;
            _factory = factory;
            _context = context;
        }

        public HttpClient Client
        {
            get
            {
                HttpClient client = _factory.CreateClient("cmsClient");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _context.HttpContext.Session.GetObject<string>(CommonConstants.TOKEN));
                return client;
            }
        }
    }
}
